FROM fedora:35

ENV ALERTMANAGER_VERSION=0.21.0

RUN dnf update -y && \
    dnf install --setopt=tsflags=nodocs -y tar gzip bind-license libssh2 vim-minimal openssl-libs && \
    dnf remove -y vim-minimal && \
    dnf clean all && \
    curl -s -S -L -o /tmp/alert_manager.tar.gz https://github.com/prometheus/alertmanager/releases/download/v$ALERTMANAGER_VERSION/alertmanager-$ALERTMANAGER_VERSION.linux-amd64.tar.gz && \
    tar -xzf /tmp/alert_manager.tar.gz && \
    mv ./alertmanager-$ALERTMANAGER_VERSION.linux-amd64/alertmanager /bin && \
    rm -rf ./alertmanager-$ALERTMANAGER_VERSION.linux-amd64 && \
    rm /tmp/alert_manager.tar.gz && \
    mkdir -p /alertmanager && \
    mkdir -p /etc/alertmanager && \
    chgrp -R root /alertmanager /etc/alertmanager && \
    chmod -R g=rwx /alertmanager /etc/alertmanager

EXPOSE      9093
WORKDIR    /alertmanager
ENTRYPOINT [ "/bin/alertmanager" ]
CMD        [ "-config.file=/etc/alertmanager/config.yml", \
             "-storage.path=/alertmanager" ]
